package com.tima.logging;

import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.tima.ai.hyperlog.HyperLog;
import com.tima.ai.hyperlog.exception.HyperLogException;

import java.util.logging.Handler;

public class RNTimalogingModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;
  private HyperLog hyperLog= null;

  public RNTimalogingModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    try {
      HyperLog.getOrNewInstanceDefault("http://52.77.231.119:6060/app-log", reactContext.getPackageName(), reactContext );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String getName() {
    return "RNTimaloging";
  }

  @ReactMethod
  public void addUID(String uid) {
    HyperLog.getInstanceDefault().getAppLogModel().uid(uid);
  }

  @ReactMethod
  public void addBehaviorLog(String key, String value) {
    HyperLog.getInstanceDefault().addBehaviorLog(key, value);
  }
  @ReactMethod
  public void pushLogToServer() {
    HyperLog.getInstanceDefault().pushLogToServer();
  }

  @ReactMethod
  public void startServiceLog() {
    HyperLog.getInstanceDefault().startServiceLog();
  }

  @ReactMethod
  public void stopServiceLog() {
    HyperLog.getInstanceDefault().stopServiceLog();
  }

}