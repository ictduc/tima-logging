using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Timaloging.RNTimaloging
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNTimalogingModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNTimalogingModule"/>.
        /// </summary>
        internal RNTimalogingModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNTimaloging";
            }
        }
    }
}
